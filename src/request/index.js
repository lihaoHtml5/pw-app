import axios from 'axios'

// 跨域
export const postHomeResource = () => {
  return axios.post('/api/HomeIndex/home')
}
export const postIndexFloor = (page2) => {
  return axios.post(`/api/GoodsClass/indexFloor?page=${page2}`)

}
export const postAd = () => {
  return axios.post('/api/Ad/getAd')
}
const ajax = axios.create({
    // baseURL:'http://mapi.pinwei100.com'
})

//请求拦截
ajax.interceptors.request.use(config=>{
    return config
})
//响应拦截
ajax.interceptors.response.use(resp=>{
    if(resp.status===200 || resp.data.status===1){
        return resp.data.data
    }else{
        return 'error'
    }
})

export const getMallLeft = ()=> ajax.get('/api/GoodsClass/getFirstId')
export const getMallRight = ({cateId}) => ajax.get(`/api/GoodsClass/getOtherClass?fid=${cateId}`)
export const getListPage = ({productId}) => ajax.get(`/api/GoodsClass/getClassGoods?page=1&store_id=0&class_three=${productId}&sort_type=desc&sort_field=update_time`)
export const getDetailPage = ({detailId}) => ajax.get(`/api/Goods/goodInfo?id=${detailId}`)
