import Vue from 'vue'
import VueX, { Store } from 'vuex'

import cart from './cart'
import user from './user'

Vue.use(VueX)

const cartPlugin = store => {
  // 当 store 初始化后调用
  store.subscribe((mutation, state) => {
    console.log({ mutation, state })
    if (mutation.type.includes('cart')) {
      // 只要提交了cart类型的mutation就重新存localStorage
      localStorage.setItem('cart', JSON.stringify(state.cart.cart))
    }
  })
}

const store = new Store({
  strict: true,
  modules: {
    cart,
    user
  },
  plugins: [cartPlugin]
})

export default store
