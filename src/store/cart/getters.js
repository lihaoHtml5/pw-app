export default{
    AllCheck(state){
        return state.cartList.every(item=>item.checked)
    },
    totalMoney(state){
        return state.cartList.reduce((money,item)=>{
            if(item.checked){
                money += item.count * item.cartProPrice  
            }
            return  money
            
        },0)
    },
    totalNum(state){
        return state.cartList.reduce((total,item)=>{
              total+=item.count
              return total
        },0)
    }
}