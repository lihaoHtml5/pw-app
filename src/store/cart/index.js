import actions from '@/store/cart/actions'
import getters from '@/store/cart/getters'
import mutations from '@/store/cart/mutations'
import state from '@/store/cart/state'

export default {
    state,
    getters,
    mutations,
    actions,
    namespaced:true,
}

// import actions from './actions'
// import getters from './getters'
// import mutations from './mutations'
// import state from './state'

// export default {
//     namespaced:true,
//     actions,
//     getters,
//     mutations,
//     state,
// }