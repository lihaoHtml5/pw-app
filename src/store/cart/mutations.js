export default{
    addToCart(state,cartInfo){
        const isHas = state.cartList.some(item=>{
            return item.cartProId === cartInfo.cartProId
        })
        console.log(isHas)
        if(isHas){
            state.cartList.map(item=>{
                if(item.cartProId === cartInfo.cartProId){
                    item.count++
                }
                return item
            })
            console.log("加数量")
        }else{
            state.cartList.push({
                ...cartInfo,
                count:1,
                checked:true
            })
            console.log("加数据")
        }
    },
    addNum(state,{cartId}){
        state.cartList = state.cartList.filter(item=>{
            console.log(item.cartProId,cartId)
            // console.log(item.cartProId==id)
            if(item.cartProId===cartId){
                item.count++
            }
            return item
        })
        
    },
    jianNum(state,{cartId}){
        state.cartList = state.cartList.filter(item=>{
            // console.log(item.cartProId==id)
            if(item.cartProId===cartId){
                item.count--
            }
            return item
        })
        
    },
    deleteOne(state,{cartId}){
       state.cartList=state.cartList.filter(item=>item.cartProId!==cartId) 
    },
    toggleCheck(state,{cartId}){
        state.cartList = state.cartList.map(item=>{
            if(item.cartProId===cartId){
                item.checked=!item.checked   
            }
            return item
            
        })
    },
    selectAll(state){
        const isAllCheck = state.cartList.every(item=>item.checked)
        state.cartList=state.cartList.map(item=>{
            item.checked= !isAllCheck
            return item
        })
    },
}