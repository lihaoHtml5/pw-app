export default {
  login (state, { username, token }) {
    state.isLogin = true
    state.username = username
    state.token = token
  }
}
