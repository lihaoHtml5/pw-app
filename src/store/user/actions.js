export default {
  // 异步 action，更新状态，
  // action 中也不会直接更新状态，而是提交 mutation
  loginAsync (context, { username, password }) {
    console.log('user action:', username, password)
    // 模拟用户登录
    return new Promise((resolve) => {
      setTimeout(() => {
        // 模拟返回响应的数据
        const data = {
          isLogin: true,
          username,
          token: 'token ' + password
        }
        // 提交 mutation 更新状态
        context.commit('login', data)
        // 返回响应数据
        resolve(data)
      }, 1000)
    })
  }
}
