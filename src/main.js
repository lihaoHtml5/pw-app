import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vant from 'vant'
import 'vant/lib/index.css'
import * as ajax from './request'

Vue.config.productionTip = false
Vue.use(vant)
Vue.prototype.$http = ajax

Vue.mixin({
  filters:{
    toFix2(num){
      return num.toFixed(2)
    },
    more99(num){
      if(num > 99){
        return '99+'
      }else{
        return num
      }
    }
  },
})
new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')


