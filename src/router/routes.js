import Home from '@/views/Home'
const Mall =()=> import('@/views/Mall')
const Cart =()=> import('@/views/Cart')
const Mine =()=> import('@/views/Mine')
const MyFooter =()=> import('@/components/MyFooter')
const listPage =()=> import('@/components/listPage')
const detailPage = ()=> import('@/components/detailPage')
const PrMall =()=> import('@/views/PrMall')
const Login = () => import('@/views/Login')
const Register = () => import('@/views/Register')

export default [
    {
        path:'/',
        redirect:'/home',
        meta:{
            isTrue:false,
        }
    },
    {
        path:'/home',
        name:'home',
        components:{
            default:Home,
            footer:MyFooter
        },
        meta:{
            isTrue:true,
            title:'首页',
            icon:'&#xe61f;',
            isback:false
        }
    },
    {
        path:'/mall',
        name:'mall',
        components:{
            default:Mall,
            footer:MyFooter
        },
        meta:{
            isTrue:true,
            title:'分类',
            icon:'&#xe60d;',
            isback:false
        },
        children:[
            {
                path:':cateId',
                name:'prmall',
                components:{
                    default:PrMall                   
                },
                meta:{
                    isback:false,
                    title:'分类'
                }
            },
            
        ]
    },
    {
        path:'/listPage/:productId',
        name:'listPage',
        components:{
            default:listPage,
            footer:MyFooter
        },
        meta:{
            isTrue:false,
            title:'商品列表',
            isback:true
        }
    },
    {
        path:'/detailPage/:detailId',
        name:'detailpage',
        components:{
            default:detailPage,
        },
        meta:{
            isTrue:false,
            title:'商品详情',
            isback:true
        }
    },
    {
        path:'/cart',
        name:'cart',
        components:{
            default:Cart,
            footer:MyFooter
        },
        meta:{
            isTrue:true,
            title:'我的购物车',
            icon:'&#xe602;',
            isback:true
        }
    },
    {
        path:'/mine',
        name:'mine',
        components:{
            default:Mine,
            footer:MyFooter
        },
        meta:{
            isTrue:true,
            title:'我的',
            icon:'&#xe658;',
            isback:false
        }
    },
    {
        path:'/login',
        name:'login',
        components:{
            default: Login
        },
        meta:{
            isTrue:false,
            title:'用户登录'
        }
    },
    {
        path:'/register',
        name:'register',
        components:{
            default: Register
        },
        meta:{
            isTrue:false,
            title:'用户注册'
        }
    }
    
]
