import Vue from 'vue'
import VueRouter from 'vue-router'
import routes from '@/router/routes'
import store from '@/store'

Vue.use(VueRouter)

const router = new VueRouter({
    mode:process.env.NODE_ENV==='development'?'hash':'history',
    routes,
})

// 需要权限访问的列表
const perimissionList = ['mine']

// 全局前置守卫，通常在全局前置守卫中判断用户权限
router.beforeEach((to, from, next) => {
  // 从状态集中获取用户是否登录
  const isLogin = store.state.user.isLogin

  if (perimissionList.includes(to.name)) { // 需要访问有权限的页面资源
    // 判断是否有用户登录
    if (isLogin) { // 如果有登录，则继续访问
      next()
    } else { // 否则（没有登录），跳转到登录页面
      next('/login')
    }
  } else { // 不需要权限，则直接访问
    next()
  }
})
export default router
